package me.koenn.gravestones.grave;

import me.koenn.gravestones.Gravestones;
import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.util.Counter;
import me.koenn.gravestones.util.OldBlock;
import me.koenn.gravestones.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Grave {

    public static ArrayList<Grave> gravestones = new ArrayList<>();
    //Config 'autoDestroy' and field.
    private static boolean autoDestroy = Boolean.parseBoolean(ConfigManager.getString("autoDestroy", "graveSettings").toUpperCase());
    private Inventory inventory;
    private Location location;
    private Counter counter;
    private String playerName;
    private Location hologramLoc;
    private Hologram hologram;
    private List<Location> clickLocs = new ArrayList<>();
    private ArrayList<OldBlock> oldBlocks = new ArrayList<>();
    private boolean isOpen;

    /**
     * Constructor for a new grave.
     * Automaticly sorts the items and creates a Counter instance
     *
     * @param contents Contents of the player's inventory
     * @param player   Player object instance
     * @param location Location where the player died
     */
    public Grave(ItemStack[] contents, Player player, Location location) {
        //Set the variables.
        this.playerName = player.getName();
        this.isOpen = false;

        //Get config settings.
        int expireTime = Integer.parseInt(ConfigManager.getString("expireTime", "graveSettings"));
        String graveName = ConfigManager.getString("graveName", "graveSettings").replace("{player}", this.playerName);

        //Make sure the graveName is not longer than 32 characters.
        if (graveName.length() > 32) {
            graveName = graveName.replace(graveName.substring(31), "");
        }

        //Check if the player has the gravestones.infinite permission.
        if (player.hasPermission("gravestones.infinite")) {

            //Set the expireTime to -1 (infinite).
            expireTime = -1;
        }


        //Construct the correct location.
        this.location = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());

        //Sort the contents and put them in the inventory.
        this.inventory = Bukkit.createInventory(null, Util.getRequiredSlots(Util.getItemAmount(contents)), graveName);
        for (ItemStack item : contents) {
            if (item == null) {
                continue;
            }
            this.inventory.addItem(item);
        }

        //Create the Counter instance.
        this.counter = new Counter(expireTime, Gravestones.getInstance());

        //Register the grave.
        gravestones.add(this);
    }

    /**
     * Open the inventory of the grave.
     *
     * @param player Player object instance
     */
    public void open(Player player) {
        if (!this.isOpen) {
            player.openInventory(this.inventory);
            this.isOpen = true;
        }
    }

    /**
     * Place the gravestone schematic and start the Counter.
     */
    public boolean place(Player player) {
        Location deathLocation = this.location.clone().add(0, -1, 0);

        //Make sure the grave does not spawn on top of an existing grave.
        while (Util.isGrave(deathLocation)) {
            deathLocation = deathLocation.add(0, 1, 0);
        }

        //Place the gravestone from the schematic.
        boolean succeed = Util.placeGravestone(deathLocation, this, player);

        //Start the Counter.
        this.counter.start(this::destroy);

        return succeed;
    }

    /**
     * Remove the hologram, clear the inventory and unregister the grave.
     */
    public void destroy() {
        //Remove the hologram of the Grave.
        if (this.hologram != null) {
            this.hologram.remove();
        }

        //Check if the autoDestroy setting is enabled.
        if (autoDestroy) {

            //Break all blocks at the clickLocs of the Grave.
            this.clickLocs.forEach(location -> location.getBlock().setType(Material.AIR));

            //Place back all the oldBlocks.
            this.oldBlocks.forEach(OldBlock::place);
        }

        //Clear the clickLocs of the Grave.
        this.clickLocs.clear();

        //Unregister the Grave.
        gravestones.remove(this);
    }

    /**
     * Drop the contents of the grave at a certain Location.
     *
     * @param dropLocation Location to drop the contents
     */
    public void dropContents(Location dropLocation) {
        for (ItemStack item : this.inventory.getContents()) {
            if (item != null) {
                Item droppedItem = dropLocation.getWorld().dropItem(dropLocation.add(0, 0.5, 0), item);
                droppedItem.setVelocity(droppedItem.getVelocity().zero());
            }
        }
    }

    /**
     * Getters and setters.
     */

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Counter getCounter() {
        return counter;
    }

    public void addClickLoc(Location location) {
        this.clickLocs.add(location);
    }

    public List<Location> getClickLocs() {
        return clickLocs;
    }

    public Location getHologramLoc() {
        return hologramLoc;
    }

    public void setHologramLoc(Location hologramLoc) {
        this.hologramLoc = hologramLoc;
    }

    public void setOldBlocks(ArrayList<OldBlock> oldBlocks) {
        this.oldBlocks = oldBlocks;
    }

    public void setHologram(Hologram hologram) {
        this.hologram = hologram;
    }
}
