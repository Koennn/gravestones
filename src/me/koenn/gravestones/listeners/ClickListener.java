package me.koenn.gravestones.listeners;

import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.grave.Grave;
import me.koenn.gravestones.util.Util;
import me.koenn.updater.client.Updater;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ClickListener implements Listener {

    //Config 'lockedToPlayer' field.
    private boolean lockedToPlayer = Boolean.parseBoolean(ConfigManager.getString("lockedToPlayer", "graveSettings").toUpperCase());

    /**
     * Listener for the PlayerInteractEvent.
     *
     * @param event PlayerInteractEvent instance
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        try {
            //Check if the Action is RIGHT_CLICK_BLOCK.
            if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                return;
            }

            //Make local variables.
            Location block = event.getClickedBlock().getLocation();

            //Loop through all Gravestones.
            for (Grave grave : Grave.gravestones) {

                //Make sure the grave exists and has a location.
                if (grave != null && grave.getClickLocs() != null && !grave.getClickLocs().isEmpty()) {

                    //Loop through the grave locations.
                    for (Location location : grave.getClickLocs()) {

                        //Make sure the location is not null.
                        if (location != null) {

                            //Check if the locations match.
                            if (location.getBlockX() == block.getBlockX() && location.getBlockY() == block.getBlockY() && location.getBlockZ() == block.getBlockZ()) {

                                //Check if the 'lockedToPlayer' option is enabled.
                                if (lockedToPlayer) {

                                    //Check if the player is the owner of the Grave.
                                    if (!event.getPlayer().getName().equals(grave.getPlayerName())) {
                                        return;
                                    }
                                }

                                //Open the inventory of the grave.
                                grave.open(event.getPlayer());

                                //Cancel the event to prevent block placement.
                                event.setCancelled(true);

                                //Loop through all Entities in the same chunk.
                                for (Entity entity : location.getWorld().getChunkAt(location).getEntities()) {

                                    //Check if the Entity is a Zombie.
                                    if (entity instanceof Zombie) {

                                        //Check if the Zombie has a custom name.
                                        if (entity.isCustomNameVisible()) {

                                            //Stop all loops.
                                            return;
                                        }
                                    }
                                }

                                //Spawn the zombie.
                                Util.spawnZombie(grave, location);

                                //Stop all loops.
                                return;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            //Report error to server.
            Updater.getUpdater().sendError(ex);
        }
    }
}
