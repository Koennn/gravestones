package me.koenn.gravestones.listeners;

import me.koenn.gravestones.Gravestones;
import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.grave.Grave;
import me.koenn.updater.client.Updater;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

import static org.bukkit.ChatColor.translateAlternateColorCodes;

public class DeathListener implements Listener {

    //Config 'notifyPlayer' and 'notifyMessage' fields.
    private boolean notifyPlayer = Boolean.parseBoolean(ConfigManager.getString("notifyPlayer", "graveSettings").toUpperCase());
    private boolean combatNoGrave = Boolean.parseBoolean(ConfigManager.getString("combatNoGrave", "graveSettings").toUpperCase());
    private String notifyMessage = translateAlternateColorCodes('&', ConfigManager.getString("notifyMessage", "graveSettings"));

    /**
     * Listener for the PlayerDeathEvent.
     *
     * @param event PlayerDeathEvent instance
     */
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        try {
            //Check if the player has the gravestones.none permission.
            if (event.getEntity().hasPermission("gravestones.none")) {
                return;
            }

            //Check if the combatNoGrave setting is enabled.
            if (combatNoGrave) {

                //Check if the player was killed by another player.
                if (event.getEntity().getKiller() != null) {

                    //Stop doing anything, don't spawn a grave.
                    return;
                }
            }

            //Get the worldBlacklist.
            List<String> worldBlacklist = ConfigManager.getList("worldBlacklist", "graveSettings");

            //Loop through the worldBlacklist.
            for (String world : worldBlacklist) {

                //Check if the player is in that world.
                if (world.equalsIgnoreCase(event.getEntity().getLocation().getWorld().getName())) {

                    //Stop doing anything, don't spawn a grave.
                    return;
                }
            }

            //Create local variables.
            Player player = event.getEntity();
            ItemStack[] items = player.getInventory().getContents();
            ItemStack[] inventory;

            //Check if the server is in 1.9 or 1.8.
            if (Gravestones.getBukkitVersion() == 1.9 || Gravestones.getBukkitVersion() == 1.10) {

                //If the server is in 1.9, simply copy the whole inventory to the Array.
                inventory = new ItemStack[items.length];
            } else if (Gravestones.getBukkitVersion() == 1.8) {

                //If the server is in 1.8, copy the whole inventory and all the armor to the Array.
                inventory = new ItemStack[items.length + 4];
                inventory[inventory.length - 4] = player.getInventory().getArmorContents()[0];
                inventory[inventory.length - 3] = player.getInventory().getArmorContents()[1];
                inventory[inventory.length - 2] = player.getInventory().getArmorContents()[2];
                inventory[inventory.length - 1] = player.getInventory().getArmorContents()[3];
            } else {

                //Throw an error if the version is null;
                throw new NullPointerException("Version cannot be null");
            }

            //Copy the ItemStack[] of the player's inventory.
            System.arraycopy(items, 0, inventory, 0, items.length);

            //Make a grave object and place it.
            Grave grave = new Grave(inventory, player, player.getLocation());
            if (grave.place(player)) {
                //Make sure the items don't drop on the ground.
                event.setKeepInventory(true);

                //Clear the player's inventory and armor.
                player.getInventory().setArmorContents(null);
                player.getInventory().clear();

                //Check if the notifyPlayer setting is enabled.
                if (notifyPlayer) {

                    //Make the coords String.
                    Location location = player.getLocation();
                    String coords = String.valueOf((int) location.getX()) + " " + String.valueOf((int) location.getY()) + " " + String.valueOf((int) location.getZ());

                    //Send a message with the coordinates of the grave to the player.
                    player.sendMessage(notifyMessage.replace("{coords}", coords));
                }
            }
        } catch (Exception ex) {
            //Report error to server.
            Updater.getUpdater().sendError(ex);
        }
    }
}
