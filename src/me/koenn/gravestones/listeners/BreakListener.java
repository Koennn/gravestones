package me.koenn.gravestones.listeners;

import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.grave.Grave;
import me.koenn.gravestones.util.Util;
import me.koenn.updater.client.Updater;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BreakListener implements Listener {

    private boolean lockedToPlayer = Boolean.parseBoolean(ConfigManager.getString("lockedToPlayer", "graveSettings").toUpperCase());

    /**
     * Listener for the BlockBreakEvent.
     *
     * @param event BlockBreakEvent instance
     */
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        try {
            //Make local variables.
            Location block = event.getBlock().getLocation();

            //Loop through all Gravestones.
            for (Grave grave : Grave.gravestones) {

                //Make sure the grave has location.
                if (!grave.getClickLocs().isEmpty()) {

                    //Loop through the grave locations.
                    for (Location location : grave.getClickLocs()) {

                        //Make sure the location is not null.
                        if (location != null) {

                            //Check if the locations match.
                            if (location.getBlockX() == block.getBlockX() && location.getBlockY() == block.getBlockY() && location.getBlockZ() == block.getBlockZ()) {

                                //Check if the 'lockedToPlayer' option is enabled.
                                if (lockedToPlayer) {

                                    //Check if the player is the owner of the Grave.
                                    if (!event.getPlayer().getName().equals(grave.getPlayerName())) {

                                        //Cancel the event.
                                        event.setCancelled(true);

                                        //Stop all loops.
                                        return;
                                    }
                                }

                                //Check if the Grave is already open.
                                if (grave.isOpen()) {

                                    //Cancel the event.
                                    event.setCancelled(true);

                                    //Stop all loops.
                                    return;
                                }

                                //Drop the contents of the grave on the ground.
                                grave.dropContents(new Location(block.getWorld(), block.getX() + 0.5, block.getY() + 0.5, block.getZ() + 0.5));

                                //Destroy the grave.
                                grave.destroy();

                                //Cancel the event to prevent the block from breaking.
                                event.setCancelled(true);

                                //Loop through all Entities in the same chunk.
                                for (Entity entity : location.getWorld().getChunkAt(location).getEntities()) {

                                    //Check if the Entity is a Zombie.
                                    if (entity instanceof Zombie) {

                                        //Check if the Zombie has a custom name.
                                        if (entity.isCustomNameVisible()) {

                                            //Stop all loops.
                                            return;
                                        }
                                    }
                                }

                                //Spawn the zombie.
                                Util.spawnZombie(grave, location);

                                //Stop all loops.
                                return;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            //Report error to server.
            Updater.getUpdater().sendError(ex);
        }
    }
}
