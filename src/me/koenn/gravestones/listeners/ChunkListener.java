package me.koenn.gravestones.listeners;

import me.koenn.gravestones.grave.Grave;
import me.koenn.updater.client.Updater;
import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

public class ChunkListener implements Listener {

    /**
     * Listener for the ChunkUnloadEvent.
     *
     * @param event ChunkUnloadEvent instance
     */
    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent event) {
        try {
            //Make local chunk variable.
            Chunk chunk = event.getChunk();

            //Loop through all Graves.
            for (Grave grave : Grave.gravestones) {

                //Get the chunk where the Grave is in.
                Chunk graveChunk;
                try {
                    graveChunk = grave.getHologramLoc().getWorld().getChunkAt(grave.getHologramLoc());
                } catch (NullPointerException ex) {
                    if (grave != null) {
                        grave.destroy();
                    }
                    return;
                }

                //Check if the chunks are the same.
                if (graveChunk.getX() == chunk.getX() && graveChunk.getZ() == chunk.getZ()) {

                    //Cancel the event.
                    event.setCancelled(true);
                }
            }
        } catch (NullPointerException ex) {
            //Report error to server.
            Updater.getUpdater().sendError(ex);
        }
    }
}
