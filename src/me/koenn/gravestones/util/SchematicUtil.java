package me.koenn.gravestones.util;

import me.koenn.gravestones.Gravestones;
import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.grave.Grave;
import me.koenn.gravestones.grave.Hologram;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.jnbt.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

@SuppressWarnings("deprecation")
public class SchematicUtil {

    //Config 'griefBypass' and 'showHologram' fields.
    private static boolean griefBypass = Boolean.parseBoolean(ConfigManager.getString("griefBypass", "graveSettings").toUpperCase());
    private static boolean showHologram = Boolean.parseBoolean(ConfigManager.getString("showHologram", "graveSettings").toUpperCase());

    /**
     * Paste a Schematic at a certain Location
     *
     * @param world     World to paste in
     * @param loc       Location to paste at
     * @param schematic Schematic to paste
     * @param grave     Grave to link the Schematic to
     */
    public static ArrayList<OldBlock> pasteSchematic(World world, Location loc, Schematic schematic, Grave grave, Player player) {
        ArrayList<OldBlock> oldBlocks = new ArrayList<>();
        byte[] blocks = schematic.getBlocks();
        byte[] blockData = schematic.getData();

        short length = schematic.getLenght();
        short width = schematic.getWidth();
        short height = schematic.getHeight();

        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                for (int z = 0; z < length; ++z) {
                    int index = y * width * length + z * width + x;
                    Block block = new Location(world, x + loc.getX(), y + loc.getY(), z + loc.getZ()).getBlock();

                    if (!griefBypass) {
                        BlockBreakEvent event = new BlockBreakEvent(block, player);
                        Bukkit.getServer().getPluginManager().callEvent(event);

                        if (event.isCancelled()) {
                            return null;
                        }
                    }


                    if (blocks[index] == 0) {
                        continue;
                    }
                    oldBlocks.add(new OldBlock(block));
                }
            }
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(Gravestones.getInstance(), () -> {
            for (int x = 0; x < width; ++x) {
                for (int y = 0; y < height; ++y) {
                    for (int z = 0; z < length; ++z) {
                        int index = y * width * length + z * width + x;
                        Block block = new Location(world, x + loc.getX(), y + loc.getY(), z + loc.getZ()).getBlock();

                        if (blocks[index] == 0) {
                            continue;
                        }

                        try {
                            if (blocks[index] > 0) {
                                block.setTypeIdAndData(blocks[index], blockData[index], true);
                            } else {
                                //Workaround for Cobblestone Walls
                                block.setTypeIdAndData(139, blockData[index], true);
                            }
                        } catch (NullPointerException ex) {
                            continue;
                        }

                        grave.addClickLoc(block.getLocation().clone());
                        if (block.getType().equals(Material.SIGN_POST)) {
                            block.setType(Material.AIR);
                            grave.setHologramLoc(block.getLocation().add(0.5, -1.6, 0.5));

                            //Make a Hologram object.
                            if (showHologram) {
                                grave.setHologram(new Hologram(grave.getHologramLoc(), grave));
                            }
                        }
                    }
                }
            }
        }, 10);
        return oldBlocks;
    }

    /**
     * Load a Schematic from a file.
     *
     * @param file Schematic file
     * @return Schematic object instance
     * @throws IOException if the file doesn't exist or is invalid
     */
    public static Schematic loadSchematic(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file);
        NBTInputStream nbtStream = new NBTInputStream(stream);

        CompoundTag schematicTag = (CompoundTag) nbtStream.readTag();
        if (!schematicTag.getName().equals("Schematic")) {
            throw new IllegalArgumentException("Tag \"Schematic\" does not exist or is not first");
        }

        Map<String, Tag> schematic = schematicTag.getValue();
        if (!schematic.containsKey("Blocks")) {
            throw new IllegalArgumentException("Schematic file is missing a \"Blocks\" tag");
        }

        short width = getChildTag(schematic, "Width", ShortTag.class).getValue();
        short length = getChildTag(schematic, "Length", ShortTag.class).getValue();
        short height = getChildTag(schematic, "Height", ShortTag.class).getValue();

        String materials = getChildTag(schematic, "Materials", StringTag.class).getValue();
        if (!materials.equals("Alpha")) {
            throw new IllegalArgumentException("Schematic file is not an Alpha schematic");
        }

        byte[] blocks = getChildTag(schematic, "Blocks", ByteArrayTag.class).getValue();
        byte[] blockData = getChildTag(schematic, "Data", ByteArrayTag.class).getValue();
        return new Schematic(blocks, blockData, width, length, height);
    }

    /**
     * Get child tag of a NBT structure.
     *
     * @param items    The parent tag map
     * @param key      The name of the tag to get
     * @param expected The expected type of the tag
     * @return child tag casted to the expected type
     * @throws IllegalArgumentException if the tag does not exist or the tag is not of the expected type
     */
    private static <T extends Tag> T getChildTag(Map<String, Tag> items, String key, Class<T> expected) throws IllegalArgumentException {
        if (!items.containsKey(key)) {
            throw new IllegalArgumentException("Schematic file is missing a \"" + key + "\" tag");
        }
        Tag tag = items.get(key);
        if (!expected.isInstance(tag)) {
            throw new IllegalArgumentException(key + " tag is not of tag type " + expected.getName());
        }
        return expected.cast(tag);
    }
}
