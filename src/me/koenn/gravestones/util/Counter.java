package me.koenn.gravestones.util;

import me.koenn.updater.client.Updater;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class Counter {

    private int timeLeft;
    private Plugin pl;
    private int task;

    /**
     * Constructor for the Counter object.
     *
     * @param timeLeft Time left on the counter
     * @param pl       Instance of the plugin
     */
    public Counter(int timeLeft, Plugin pl) {
        this.timeLeft = timeLeft;
        this.pl = pl;
    }

    /**
     * Start the counter and call the callback when the time runs out.
     *
     * @param callback Runnable to run when the time runs out
     */
    public void start(Runnable callback) {
        //Check if the time is set to -1.
        if (timeLeft == -1) {
            return;
        }

        //Start a repeating task to run every second.
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, () -> {
            try {
                //Decrease the time left.
                timeLeft--;

                //Run the callback and stop the task when the time runs out.
                if (timeLeft == 0) {
                    callback.run();
                    Bukkit.getScheduler().cancelTask(task);
                }
            } catch (Exception ex) {
                //Report error to server.
                Updater.getUpdater().sendError(ex);
            }
        }, 0, 20);
    }

    /**
     * Get the time left on the counter.
     *
     * @return int timeLeft
     */
    public int getTimeLeft() {
        return timeLeft;
    }

}
