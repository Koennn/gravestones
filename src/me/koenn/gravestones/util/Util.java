package me.koenn.gravestones.util;

import me.koenn.gravestones.Gravestones;
import me.koenn.gravestones.config.ConfigManager;
import me.koenn.gravestones.grave.Grave;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.bukkit.ChatColor.translateAlternateColorCodes;

public class Util {

    /**
     * Config fields.
     */
    private static boolean spawnZombie = Boolean.parseBoolean(ConfigManager.getString("spawnZombie", "graveSettings"));
    private static double zombieChance = Double.parseDouble(ConfigManager.getString("zombieChance", "graveSettings"));
    private static String zombieName = translateAlternateColorCodes('&', ConfigManager.getString("zombieName", "graveSettings"));
    private static double zombieHealth = Double.parseDouble(ConfigManager.getString("zombieHealth", "graveSettings"));

    /**
     * Place a gravestone at a certain Location and link it to a Grave.
     *
     * @param deathLocation Location to paste the Schematic at
     * @param grave         Grave to link to the Schematic
     */
    public static boolean placeGravestone(Location deathLocation, Grave grave, Player player) {
        //Get the schematic file.
        File schematicFile = new File(Gravestones.getInstance().getDataFolder(), "gravestone.schematic");

        //Check if the block below the location is air or water, if so, get the gravestone_no_gravity schematic file.
        if (deathLocation.clone().add(0, -1, 0).getBlock().getType().equals(Material.AIR) || deathLocation.clone().add(0, -1, 0).getBlock().getType().name().contains("WATER")) {
            schematicFile = new File(Gravestones.getInstance().getDataFolder(), "gravestone_no_gravity.schematic");
        }

        //Check if the file exists.
        if (!schematicFile.exists()) {
            return false;
        }

        try {
            //Load the Schematic.
            Schematic schematic = SchematicUtil.loadSchematic(schematicFile);

            //Paste the Schematic at the given location.
            ArrayList<OldBlock> oldBlocks = SchematicUtil.pasteSchematic(deathLocation.getWorld(), deathLocation, schematic, grave, player);
            try {
                grave.setOldBlocks(oldBlocks);
            } catch (NullPointerException ex) {
                return false;
            }
            return oldBlocks != null;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get the line index containing the {minutes} placeholder.
     *
     * @return int index
     */
    public static int getTimeFormatLine() {
        //Get the list of all the lines.
        List<String> lines = ConfigManager.getList("hologramLayout", "graveSettings");

        //Loop through all the lines.
        for (int i = 0; i < lines.size(); i++) {

            //Check if the line contains the {minutes} variable.
            if (lines.get(i).contains("{minutes}")) {

                //Return the line index.
                return i;
            }
        }

        //If there is no {minutes} variable, return the last line index.
        return lines.size() - 1;
    }

    /**
     * Get the line containing the {minutes} placeholder.
     *
     * @return String line
     */
    public static String getTimeLine() {
        //Get the list of all the lines.
        List<String> lines = ConfigManager.getList("hologramLayout", "graveSettings");

        //Loop through all the lines.
        for (String line : lines) {

            //Check if the line contains the {minutes} variable.
            if (line.contains("{minutes}")) {

                //Return the line index.
                return line;
            }
        }

        //If there is no {minutes} variable, return the last line.
        return lines.get(lines.size() - 1);
    }

    /**
     * Check if an Inventory is emtpy.
     *
     * @param inventory Inventory object
     * @return boolean isEmtpy
     */
    public static boolean isEmpty(Inventory inventory) {
        //Loop through all the items in the inventory.
        for (ItemStack item : inventory.getContents()) {

            //Check if the item is not null.
            if (item != null) {

                //If it isn't null, return false.
                return false;
            }
        }

        //If all items are null, return true.
        return true;
    }

    /**
     * Get the amount of Inventory slots required for a certain amount of items.
     *
     * @param itemAmount Amount of items needed to store
     * @return int amount of slots needed in the inventory
     */
    public static int getRequiredSlots(int itemAmount) {
        //Check if the item amount is 0.
        if (itemAmount == 0) {
            //Return 9 slots (1 row).
            return 9;
        }

        //Make the slots variable and set it to the itemAmount.
        int slots = itemAmount;

        //Keep looping until you can divide slots by 9.
        while (slots % 9 != 0) {

            //Increase slots by 1.
            slots++;
        }

        //Return slots.
        return slots;
    }

    /**
     * Get the amount of items in an ItemStack[].
     *
     * @param items ItemStack[]
     * @return int amount of items
     */
    public static int getItemAmount(ItemStack[] items) {
        //Set the amount variable to 0.
        int amount = 0;

        //Loop through all the items.
        for (ItemStack item : items) {

            //Check if the item isn't null.
            if (item != null) {

                //Increase the amount by 1.
                amount++;
            }
        }

        //Return the amount.
        return amount;
    }

    /**
     * Check if there is a Grave at a certain Location.
     *
     * @param location Location object
     * @return boolean isGrave
     */
    public static boolean isGrave(Location location) {
        //Loop through all Graves.
        for (Grave grave : Grave.gravestones) {

            //Make sure the Grave is not null.
            if (grave == null || grave.getHologramLoc() == null) {
                continue;
            }

            //Make sure the Grave is in the same world as the Location.
            if (!grave.getHologramLoc().getWorld().getName().equals(location.getWorld().getName())) {
                continue;
            }

            //Check if the Location is within 5 blocks of the Grave.
            if (grave.getHologramLoc().distance(location) < 5) {
                return true;
            }
        }
        return false;
    }

    /**
     * Spawn a Zombie with the right chance at a certain Location.
     *
     * @param grave    Grave to spawn the Zombie on
     * @param location Location object
     */
    public static void spawnZombie(Grave grave, Location location) {
        //Check if the spawnZombie setting is enabled.
        if (spawnZombie) {

            //Check the zombieChance chance.
            if (new Random().nextInt(100) < zombieChance) {

                //Spawn the zombie with all its properties.
                LivingEntity zombie = (LivingEntity) location.getWorld().spawnEntity(location.clone().add(0.5, 1, 0.5), EntityType.ZOMBIE);
                zombie.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 1000 * 1000, 0, true, false));
                zombie.setCustomName(zombieName.replace("{player}", grave.getPlayerName()));
                zombie.setCustomNameVisible(true);
                zombie.setMaxHealth(zombieHealth);
                zombie.setHealth(zombieHealth);
            }
        }
    }
}
